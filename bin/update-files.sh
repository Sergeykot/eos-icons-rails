#!/bin/bash

## Update all assets

wget https://gitlab.com/SUSE-UIUX/eos-icons/-/raw/master/dist/css/_eos-icons.scss -q -O ./vendor/assets/stylesheets/eos-icons-font.scss
wget https://gitlab.com/SUSE-UIUX/eos-icons/-/raw/master/dist/css/eos-icons.less -q -O ./vendor/assets/stylesheets/eos-icons-font.less
wget https://gitlab.com/SUSE-UIUX/eos-icons/-/raw/master/dist/fonts/eos-icons.eot -q -O ./vendor/assets/fonts/eos-icons.eot
wget https://gitlab.com/SUSE-UIUX/eos-icons/-/raw/master/dist/fonts/eos-icons.svg -q -O ./vendor/assets/fonts/eos-icons.svg
wget https://gitlab.com/SUSE-UIUX/eos-icons/-/raw/master/dist/fonts/eos-icons.ttf -q -O ./vendor/assets/fonts/eos-icons.ttf
wget https://gitlab.com/SUSE-UIUX/eos-icons/-/raw/master/dist/fonts/eos-icons.woff -q -O ./vendor/assets/fonts/eos-icons.woff
wget https://gitlab.com/SUSE-UIUX/eos-icons/-/raw/master/dist/fonts/eos-icons.woff2 -q -O ./vendor/assets/fonts/eos-icons.woff2

git add ./vendor/assets/stylesheets/eos-icons-font.scss
git add ./vendor/assets/stylesheets/eos-icons-font.less
git add ./vendor/assets/fonts/eos-icons.eot
git add ./vendor/assets/fonts/eos-icons.svg
git add ./vendor/assets/fonts/eos-icons.ttf
git add ./vendor/assets/fonts/eos-icons.woff
git add ./vendor/assets/fonts/eos-icons.woff2

## Get the package
wget https://gitlab.com/SUSE-UIUX/eos-icons/-/raw/master/package.json -q -O ./package.json

## And detect the new version of the NPM
NEWVERSION=$(grep -o '"version": "[^"]*' ./package.json | grep -o '[^"]*$')

## Delete the current gemspec
rm eos-icons-font.gemspec
## And create a new gemspec from the template
cp ./eos-icons-font.template.gemspec ./eos-icons-font.gemspec

## And add the same version number as that of the package.json (the npm)
sed -i 's/{{NEW_VERSION_GEM}}/'"$NEWVERSION"'/g' eos-icons-font.gemspec
git add ./eos-icons-font.gemspec

## Make a commit with the version number
git commit -m "Update to version '$NEWVERSION'"
git push

### Create a version tag in Git
git tag "v'$NEWVERSION"
git push --tags

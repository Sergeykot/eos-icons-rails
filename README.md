# eos-icons-rails

This is the source for the [eos-icons-font](https://rubygems.org/gems/eos-icons-font) gem which wraps the [EOS icons](https://gitlab.com/SUSE-UIUX/eos-icons) library in a Rails engine for simple use with the asset pipeline provided by Rails 3.1 and higher.

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'eos-icons-font'
```

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install eos-icons-font

## Usage

Add the following directive to your `application.css`:

```
*= require eos-icons-font
```

## Contributing

Bug reports and pull requests regarding EOS icons should go to https://gitlab.com/SUSE-UIUX/eos-icons.
If you find an issue with how this gem works, report it here: https://github.com/thutterer/eos-icons-rails.


## License

The gem is available as open source under the terms of the [MIT License](http://opensource.org/licenses/MIT).
